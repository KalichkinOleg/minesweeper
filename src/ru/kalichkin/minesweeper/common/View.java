package ru.kalichkin.minesweeper.common;

import ru.kalichkin.minesweeper.model.Action;
import ru.kalichkin.minesweeper.model.Field;

import java.io.IOException;


public interface View {
    void startApplication();

    void onDraw(Field field) throws IOException;

    void setViewListener(ViewListener listener);

    Action onAction(int button);

    void onDefeat();

    String onIsWin();
}
